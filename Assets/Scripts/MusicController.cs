﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Toggle))]
public class MusicController : MonoBehaviour {


    private Toggle tgl;
    private Image OffImage;
    private string tglName;

    void Awake()
    {
        tgl = this.GetComponent<Toggle>();
        tglName = this.transform.name;
        OffImage = this.transform.GetChild(0).Find("OffImage").GetComponent<Image>();
    }

    void Start()
    {
        if (PlayerPrefs.GetInt(tglName, 1) == 1)
        {
            tgl.isOn = true;
            OffImage.color = new Color(1f, 1f, 1f, 0f);
        }
        else
        {
            tgl.isOn = false;
            AudioController.SetCategoryVolume(tglName, 0f);
        }
    }

    public void OnToggleClick()
    {
        if (tgl.isOn)
        {
            PlayerPrefs.SetInt(tglName, 1);
            AudioController.SetCategoryVolume(tglName, 1f);
            OffImage.DOFade(0f, 0.2f);
            FlurryServiceScript.Instance.LogFlurryEvent(tglName + " toggle click", new Dictionary<string, string> { { "Turned: ", "On" } });
        }
        else
        {
            PlayerPrefs.SetInt(tglName, 0);
            AudioController.SetCategoryVolume(tglName, 0f);
            OffImage.DOFade(1f, 0.2f);
            FlurryServiceScript.Instance.LogFlurryEvent(tglName + " toggle click", new Dictionary<string, string> { { "Turned: ", "Off" } });
        }
    }

}
