﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;

public class MenuMovement : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool isModesPanel;
    public RectTransform[] Panels;
    public Toggle[] navigationToggles;
    public RectTransform[] navigationRTs;
    protected float[] initXPositions;
    protected int firstFrame = 1;
    protected int currentFrame = 1;

    private float screenWidth = 1080f;
    private int camWidth;

    private float swipeBeginTime = 0f;

    void Start()
    {
        camWidth = Camera.main.pixelWidth;
        initXPositions = new float[Panels.Length];
        for(int i=0; i<Panels.Length; i++)
        {
            initXPositions[i] = Panels[i].anchoredPosition.x;
        }
        if (isModesPanel)
        {
            currentFrame = PlayerPrefs.GetInt("MODES_POSITION", 1);
            if (currentFrame != firstFrame)
            {
                for (int i = 0; i < Panels.Length; i++)
                {
                    Panels[i].anchoredPosition += new Vector2(screenWidth * (firstFrame - currentFrame), 0f);
                }
                firstFrame = currentFrame;
                ModesController.Instance.SetCurrentMode(currentFrame);
            }
        }
        navigationToggles[currentFrame].isOn = true;
        navigationRTs = new RectTransform[navigationToggles.Length];

        for(int i=0; i<navigationRTs.Length; i++)
        {
            navigationRTs[i] = navigationToggles[i].GetComponent<RectTransform>();
        }
        navigationRTs[currentFrame].sizeDelta = new Vector2(60f, 60f);
    }

    protected Vector2 pressPos;

    public void OnBeginDrag(PointerEventData eventData)
    {
        for(int i=0; i<Panels.Length; i++)
        {
            DOTween.Kill(Panels[i]);
        }
        pressPos = eventData.pressPosition;
        swipeBeginTime = Time.time;
        if (isModesPanel)
        {
            GUIManager.Instance.SetModesButtonInteractable(false);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        for(int i=0; i<Panels.Length; i++)
        {
            Panels[i].anchoredPosition += new Vector2(screenWidth * eventData.delta.x / camWidth, 0f);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        int prevFrame = currentFrame;
        float swipeTime = Mathf.Clamp01(Time.time - swipeBeginTime); // this variable was added to make smooth transition
        if (Panels[currentFrame].anchoredPosition.x > swipeTime * screenWidth / 2f && currentFrame > 0)
        {
            /*If swipe on more then third of screen move to the left*/
            currentFrame--;
        }
        else if (Panels[currentFrame].anchoredPosition.x < -swipeTime * screenWidth / 2f && (currentFrame < Panels.Length - 1))
        {
            /*If swipe on more then third of screen to the right*/
            currentFrame++;
        }
        if(prevFrame != currentFrame)
        {
            navigationToggles[currentFrame].isOn = true;
            DOTween.Kill(navigationRTs[prevFrame]);
            DOTween.Kill(navigationRTs[currentFrame]);
            navigationRTs[prevFrame].DOSizeDelta(new Vector2(50f, 50f), 0.25f).SetEase(Ease.InOutBounce);
            navigationRTs[currentFrame].DOSizeDelta(new Vector2(60f, 60f), 0.25f).SetEase(Ease.InOutBounce);
        }
        for (int i = 0; i < Panels.Length; i++)
        {
            Panels[i].DOAnchorPosX((i - currentFrame) * screenWidth, 0.5f).SetEase(Ease.OutBack).SetUpdate(true);
        }
        //Debug.Log("Current frame = " + currentFrame);
        if (isModesPanel)
        {
            ModesController.Instance.SetGameMode(currentFrame);
            GUIManager.Instance.SetModesButtonInteractable(true);
        }
    }

    public void SetInitialPositions()
    {
        if(currentFrame != firstFrame)
        {
            DOTween.Kill(navigationRTs[currentFrame]);
            DOTween.Kill(navigationRTs[firstFrame]);
            navigationToggles[currentFrame].isOn = false;
            navigationRTs[currentFrame].sizeDelta = new Vector2(50f, 50f);
            currentFrame = firstFrame;
            navigationToggles[currentFrame].isOn = true;
            navigationRTs[currentFrame].sizeDelta = new Vector2(60f, 60f);
            for (int i = 0; i < Panels.Length; i++)
            {
                DOTween.Kill(Panels[i]);
                Panels[i].DOAnchorPosX((i - currentFrame) * screenWidth, 0.5f).SetEase(Ease.OutBack).SetUpdate(true);
            }
        }
    }

    private void OnDisable()
    {
        if (isModesPanel)
        {
            PlayerPrefs.SetInt("MODES_POSITION", currentFrame);
        }
    }
}
