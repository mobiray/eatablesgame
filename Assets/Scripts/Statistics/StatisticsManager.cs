﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StatisticsManager : MonoBehaviour {

    private string NEW_STAT = "FreshStat";
    private string ALL_STAT = "AllStat";

    public class NewStatistics
    {
        public List<NewStatLine> stat = new List<NewStatLine>();
    }

    [Serializable]
    public class NewStatLine
    {
        public string date;
        public int score;

        public NewStatLine() {}

        public NewStatLine(string _date, int _score)
        {
            date = _date;
            score = _score;
        }

        public DateTime ToDateTime()
        {
            string[] dateSplit = date.Split('/');
            return new DateTime(int.Parse(dateSplit[2]), int.Parse(dateSplit[0]), int.Parse(dateSplit[1]));
        }
    }

    public class OverallStat
    {
        public int gamesCount;
        public float averageScore;

        public void AddLine(int _score)
        {
            averageScore = (averageScore * gamesCount + _score) / (gamesCount + 1);
            gamesCount++;
        }
    }

    public NewStatistics NStat { get; protected set; }
    public OverallStat AllStat { get; protected set; }
    private DateTime today;
    public  int todayGames { get; protected set; }
    public  int weekGames { get; protected set; }
    public int todayBestScore { get; protected set; }
    public int weekBestScore { get; protected set; }
    public int todayScoreSum { get; protected set; }
    public int weekScoreSum { get; protected set; }

    static public StatisticsManager Instance { get { return _instance; } }
    static protected StatisticsManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        CheckOldStatistics();
    }

    public void AddNewLine(int newScore)
    {
        if(todayBestScore < newScore)
        {
            todayBestScore = newScore;
            if(weekBestScore < newScore)
            {
                weekBestScore = newScore;
            }
        }
        todayScoreSum += newScore;
        weekScoreSum += newScore;
        todayGames++;
        weekGames++;
        NewStatLine newStat = new NewStatLine(DateTime.Today.ToShortDateString(), newScore);
        NStat.stat.Add(newStat);
        AllStat.AddLine(newScore);
        SaveNewStatistics();
        StatisticsGUIManager.Instance.UpdateStatisticsGUI();
    }

    private void SaveNewStatistics()
    {
        String jsonString = JsonUtility.ToJson(NStat);
        PlayerPrefs.SetString(NEW_STAT + (int)ModesController.Instance.currentMode, jsonString);
        jsonString = JsonUtility.ToJson(AllStat);
        PlayerPrefs.SetString(ALL_STAT + (int)ModesController.Instance.currentMode, jsonString);
    }

    public void LoadStatistics()
    {
        AllStat = new OverallStat();
        if (PlayerPrefs.HasKey(ALL_STAT + (int)ModesController.Instance.currentMode))
        {
            AllStat = JsonUtility.FromJson<OverallStat>(PlayerPrefs.GetString(ALL_STAT + (int)ModesController.Instance.currentMode, ""));
        }
        NStat = new NewStatistics();
        if (PlayerPrefs.HasKey(NEW_STAT + (int)ModesController.Instance.currentMode))
        {
            NStat = JsonUtility.FromJson<NewStatistics>(PlayerPrefs.GetString(NEW_STAT + (int)ModesController.Instance.currentMode, ""));
        }
        todayBestScore = 0;
        weekBestScore = 0;
        todayScoreSum = 0;
        weekScoreSum = 0;
        todayGames = 0;
        weekGames = 0;
        /**********************************/
        //DateTime date1 = DateTime.Now;
        //date1 = date1.AddDays(-7);
        //DateTime date2 = date1.AddDays(-1);
        //NewStatLine stat1 = new NewStatLine();
        //stat1.date = date1.ToShortDateString();
        //stat1.score = 120;
        //NewStatLine stat2 = new NewStatLine();
        //stat2.date = date2.ToShortDateString();
        //stat2.score = 200;
        //NStat.stat.Add(stat1);
        //NStat.stat.Add(stat2);
        //AllStat.AddLine(stat1.score);
        //AllStat.AddLine(stat2.score);
        //SaveNewStatistics();
        /**********************************/
        bool shouldUpdate = false;
        today = DateTime.Today;
        if (NStat != null)
        {
            for(int i=0; i<NStat.stat.Count; i++)
            {
                if(NStat.stat[i].ToDateTime() > today.AddDays(-7))
                {
                    /*This game is less than a week*/
                    weekGames++;
                    weekScoreSum += NStat.stat[i].score;
                    if (weekBestScore < NStat.stat[i].score)
                    {
                        weekBestScore = NStat.stat[i].score;
                    }
                    if(NStat.stat[i].ToDateTime() == today)
                    {
                        /*This is today game*/
                        todayGames++;
                        todayScoreSum += NStat.stat[i].score;
                        if (todayBestScore < NStat.stat[i].score)
                        {
                            todayBestScore = NStat.stat[i].score;
                        }
                    }
                } else
                {
                    NStat.stat.RemoveAt(i);
                    i--;
                    shouldUpdate = true;
                }
            }
            if (shouldUpdate)
            {
                SaveNewStatistics();
            }
        }
        StatisticsGUIManager.Instance.UpdateStatisticsGUI();
    }

    private void CheckOldStatistics()
    {
        if(PlayerPrefs.HasKey(ALL_STAT) || PlayerPrefs.HasKey(NEW_STAT))
        {
            PlayerPrefs.SetString(ALL_STAT + "2", PlayerPrefs.GetString(ALL_STAT, ""));
            PlayerPrefs.DeleteKey(ALL_STAT);
            PlayerPrefs.SetString(NEW_STAT + "2", PlayerPrefs.GetString(NEW_STAT, ""));
            PlayerPrefs.DeleteKey(NEW_STAT);
        }
    }
    //void OnApplicationQuit()
    //{
    //    PlayerPrefs.DeleteAll();
    //}
}
