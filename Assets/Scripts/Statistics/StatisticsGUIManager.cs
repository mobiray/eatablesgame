﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class StatisticsGUIManager : MonoBehaviour {

    [Header("Games played text")]
    public Text gpToday;
    public Text gpWeek;
    public Text gpAll;

    [Space(10)]
    [Header("Best score text")]
    public Text bsToday;
    public Text bsWeek;
    public Text bsAll;

    [Space(10)]
    [Header("Average score text")]
    public Text asToday;
    public Text asWeek;
    public Text asAll;

    private StatisticsManager sm;

    static public StatisticsGUIManager Instance { get { return _instance; } }
    static protected StatisticsGUIManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void UpdateStatisticsGUI()
    {
        sm = StatisticsManager.Instance;
        gpToday.text = Convert.ToString(sm.todayGames);
        gpWeek.text = Convert.ToString(sm.weekGames);
        gpAll.text = Convert.ToString(sm.AllStat.gamesCount);

        if(ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
        {
            bsToday.text = Convert.ToString(sm.todayBestScore / 10f) + "%";
            bsWeek.text = Convert.ToString(sm.weekBestScore / 10f) + "%";
            bsAll.text = Convert.ToString(PlayerPrefs.GetInt("BestScore" + (int)ModesController.Instance.currentMode, 0) / 10f) + "%";
        } else
        {
            bsToday.text = Convert.ToString(sm.todayBestScore);
            bsWeek.text = Convert.ToString(sm.weekBestScore);
            bsAll.text = Convert.ToString(PlayerPrefs.GetInt("BestScore" + (int)ModesController.Instance.currentMode, 0));
        }

        if (sm.todayGames != 0)
        {
            // asToday.text = Convert.ToString((float)sm.todayScoreSum / sm.todayGames);
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asToday.text = Convert.ToString(Math.Round((float)sm.todayScoreSum / sm.todayGames, 1) / 10f) + "%";
            }
            else
            {
                asToday.text = Convert.ToString(Math.Round((float)sm.todayScoreSum / sm.todayGames, 2));
            }
        }
        else
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asToday.text = "0%";
            }
            else
            {
                asToday.text = "0";
            }
        }
        if (sm.weekGames != 0)
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asWeek.text = Convert.ToString(Math.Round((float)sm.weekScoreSum / sm.weekGames, 1) / 10f) + "%";
            }
            else
            {
                asWeek.text = Convert.ToString(Math.Round((float)sm.weekScoreSum / sm.weekGames, 2));
            }
            
        }
        else
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asWeek.text = "0%";
            }
            else
            {
                asWeek.text = "0";
            }
        }
        if(sm.AllStat != null)
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asAll.text = Convert.ToString(Math.Round(sm.AllStat.averageScore, 1) / 10f) + "%";
            }
            else
            {
                asAll.text = Convert.ToString(Math.Round(sm.AllStat.averageScore, 2));
            }
        } else
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
            {
                asAll.text = "0%";
            }
            else
            {
                asAll.text = "0";
            }
        }
    }
}
