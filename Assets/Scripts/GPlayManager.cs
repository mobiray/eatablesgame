﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GPlayManager : MonoBehaviour {

    private int[] roundScoreAchiev = { 10, 20, 30, 40, 45, 50};
    private int[] totalGamesAciev = { 5, 10, 25};
    private bool isAuthenticate = false;

    static public GPlayManager Instance { get { return _instance; } }
    static protected GPlayManager _instance;

    void Awake()
    {
        _instance = this;
        //PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
        //PlayGamesPlatform.InitializeInstance(config);
    }

    void Start()
    {
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) =>
        {
            isAuthenticate = success;
        });
    }

    public void ReportScore(int score)
    {
        if (Social.localUser.authenticated)
        {
            if (ModesController.Instance.currentMode == ModesController.GameMode.ShortGame)
            {
                Social.ReportScore(score, "CgkItImnqPUZEAIQCg", (bool repSuccess) =>
                {
                    // handle success or failure
                });
            } else if(ModesController.Instance.currentMode == ModesController.GameMode.LongGame)
            {
                Social.ReportScore(score, "CgkItImnqPUZEAIQCw", (bool repSuccess) =>
                {
                    // handle success or failure
                });
            }
           
        }
    }

    public void ShowLeaderboardUI()
    {
        Social.localUser.Authenticate((bool success) => {
            isAuthenticate = success;
            if (success)
            {
                switch (ModesController.Instance.currentMode)
                {
                    case ModesController.GameMode.NoTimeLimit:
                        Social.ShowLeaderboardUI();
                        break;
                    case ModesController.GameMode.ShortGame:
                        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI("CgkItImnqPUZEAIQCg");
                        break;
                    case ModesController.GameMode.LongGame:
                        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI("CgkItImnqPUZEAIQCw");
                        break;
                }
                //PlayGamesPlatform.Instance.ShowLeaderboardUI("CgkItImnqPUZEAIQCg");
            }
        });
    }

    public void ShowAchievmentsUI()
    {
        Social.localUser.Authenticate((bool success) => {
            isAuthenticate = success;
            if (success)
            {
                //Social.ShowAchievementsUI();
                ((PlayGamesPlatform)Social.Active).ShowAchievementsUI();
            }
        });
    }

    public void UnlockRoundScoreAchievment(int score)
    {
        if (Social.localUser.authenticated)
        {
            if(ModesController.Instance.currentMode == ModesController.GameMode.ShortGame)
            {
                if (score >= roundScoreAchiev[0])
                {
                    Social.ReportProgress("CgkItImnqPUZEAIQAQ", 100.0f, (bool achSuccess) => {
                    });
                    if (score >= roundScoreAchiev[1])
                    {
                        Social.ReportProgress("CgkItImnqPUZEAIQAg", 100.0f, (bool achSuccess) => {
                        });
                        if (score >= roundScoreAchiev[2])
                        {
                            Social.ReportProgress("CgkItImnqPUZEAIQAw", 100.0f, (bool achSuccess) => {
                            });
                            if (score >= roundScoreAchiev[3])
                            {
                                Social.ReportProgress("CgkItImnqPUZEAIQBA", 100.0f, (bool achSuccess) => {
                                });
                                if (score >= roundScoreAchiev[4])
                                {
                                    Social.ReportProgress("CgkItImnqPUZEAIQBQ", 100.0f, (bool achSuccess) => {
                                    });
                                    if (score >= roundScoreAchiev[5])
                                    {
                                        Social.ReportProgress("CgkItImnqPUZEAIQBg", 100.0f, (bool achSuccess) => {
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            } else if(ModesController.Instance.currentMode == ModesController.GameMode.LongGame)
            {
                if (score >= 100)
                {
                    Social.ReportProgress("CgkItImnqPUZEAIQDg", 100.0f, (bool achSuccess) =>
                    {
                    });
                    if(score >= 110)
                    {
                        Social.ReportProgress("CgkItImnqPUZEAIQDQ", 100.0f, (bool achSuccess) =>
                        {
                        });
                    }
                }
            }
            
        }

    }

    public void UnlockTotalGamesAchievment()
    {
        if (Social.localUser.authenticated)
        {
            int gamesCount = StatisticsManager.Instance.AllStat.gamesCount;
            if(gamesCount > 60)
            {
                gamesCount = 60;
            }
            if(ModesController.Instance.currentMode == ModesController.GameMode.ShortGame)
            {
                PlayGamesPlatform.Instance.IncrementAchievement("CgkItImnqPUZEAIQBw", 1, (bool success) => { });
                PlayGamesPlatform.Instance.IncrementAchievement("CgkItImnqPUZEAIQCA", 1, (bool success) => { });
                PlayGamesPlatform.Instance.IncrementAchievement("CgkItImnqPUZEAIQCQ", 1, (bool success) => { });
            } else if(ModesController.Instance.currentMode == ModesController.GameMode.LongGame)
            {
                PlayGamesPlatform.Instance.IncrementAchievement("CgkItImnqPUZEAIQDw", 1, (bool success) => { });
                PlayGamesPlatform.Instance.IncrementAchievement("CgkItImnqPUZEAIQEA", 1, (bool success) => { });
            }
           
        }
    }
}
