﻿using UnityEngine;
using System.Collections;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine.UI;
using System;

public class AdvertismentScript : MonoBehaviour
{
    public float LastADSTime = -30f;

    static public AdvertismentScript Instance
    {
        get { return _instance; }
    }

    static protected AdvertismentScript _instance;

    private string appKey = "d50ad3c0b033e62d816c9f33ac2edf5adae155ec4b8f3d6e";

    private void Awake()
    {
        if (PlayerPrefs.GetInt("IsFirstRun", 0) == 0)
        {
            PlayerPrefs.SetInt("IsFirstRun", 1);
            LastADSTime += 150f;
        }
    }

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        Appodeal.disableLocationPermissionCheck();
        Appodeal.disableNetwork("ogury");

        Init();
    }

    private bool isInitialized = false;

    public void Init()
    {
        if (!isInitialized)
        {
            if (PlayerPrefs.HasKey("result_gdpr"))
            {
                if (PlayerPrefs.GetInt("result_gdpr") == 1)
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_TOP, true);
                    isInitialized = true;
                }
                else
                {
                    Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_TOP, false);
                    isInitialized = true;
                }
            }
            else if (PlayerPrefs.GetInt("isNotNeedtoShowGPDR") == 1)
            {
                Appodeal.initialize(appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_TOP, true);
                isInitialized = true;
            }

            if (isInitialized)
            {
                ShowBannerTop();
                Debug.Log("Show banner top");
            }
        }
        else
        {
            Debug.Log("appodealinit fe");
        }
    }

    public void ShowInterstitial()
    {
        if (isInterstitialLoaded() && (Time.realtimeSinceStartup >= LastADSTime + 180f))
        {
            AudioController.Stop("IncorrectSound");
            LastADSTime = Time.realtimeSinceStartup;
            Appodeal.show(Appodeal.INTERSTITIAL);
        }
    }

    public bool isInterstitialLoaded()
    {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

    public void ShowBannerTop()
    {
        Appodeal.show(Appodeal.BANNER_TOP);
    }

    public void HideBannerTop()
    {
        Appodeal.hide(Appodeal.BANNER_TOP);
    }
}