﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class CardsManager : MonoBehaviour {
    
    public SpriteRenderer cardSprite;

    private Card cardSample;
    public Card playedCard { get; protected set; }

    public class Card
    {
        public string imgName;
        public string imgPath;
        public Sprite cardImg;

        public Card(string _name, string _path)
        {
            imgName = _name;
            imgPath = _path;
            //cardImg = Resources.Load<Sprite>("CardsLibrary/" + imgPath + "/" + imgName);
        }

        public Card(string _name, string _path, Sprite _sprite)
        {
            imgName = _name;
            imgPath = _path;
            cardImg = _sprite;
        }


    }

    public class Deck
    {
        public List<Card> cards = new List<Card>();
    }

    protected Deck deckModel = new Deck();
    public Deck currentDeck { get; protected set; }
    public Deck releaseDeck { get; protected set; }

    private Color32 tmpAlpha0 = new Color(255, 255, 255, 0);

    public bool isFirstTouch = true;
    static public CardsManager Instance { get { return _instance; } }
    static protected CardsManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        int eatableCardsCount = 57;
        int uneatableCardsCount = 57;
        //Sprite[] eatableSprites = Resources.LoadAll<Sprite>("CardsLibrary/Eatable/");
        //Sprite[] uneatableSprites = Resources.LoadAll<Sprite>("CardsLibrary/Uneatable/");
        for (int i=0; i< eatableCardsCount; i++)
        {
            Card newCard = new Card(Convert.ToString(i), "Eatable");
            //Card newCard = new Card(Convert.ToString(i), "Eatable", eatableSprites[i]);
            deckModel.cards.Add(newCard);
        }
        for (int i = 0; i < uneatableCardsCount; i++)
        {
            Card newCard = new Card(Convert.ToString(i), "Uneatable");
            //Card newCard = new Card(Convert.ToString(i), "Uneatable", uneatableSprites[i]);
            deckModel.cards.Add(newCard);
        }
        //eatableSprites = null;
        //uneatableSprites = null;
        //cardSR = cardGO.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        GetNewCurrentDeck();
        PlayCard();
    }

    private void GetNewCurrentDeck()
    {
        Deck tmpDeck = new Deck();
        for(int i=0; i<deckModel.cards.Count; i++)
        {
            tmpDeck.cards.Add(deckModel.cards[i]);
        }
        currentDeck = new Deck();
        //currentDeck = tmpDeck;
        int cardsCount = tmpDeck.cards.Count;
        int cardID = 0;
        for (int i = 0; i < cardsCount; i++)
        {
            cardID = UnityEngine.Random.Range(0, tmpDeck.cards.Count);
            cardSample = new Card(tmpDeck.cards[cardID].imgName, tmpDeck.cards[cardID].imgPath);
            tmpDeck.cards.RemoveAt(cardID);
            currentDeck.cards.Add(cardSample);
            //  Debug.Log(i + "th card in deck is " + cardSample.imgPath + ", " + cardSample.imgName);
        }
        tmpDeck = null;
        releaseDeck = new Deck();
        //Debug.Log("*******************");
    }

    private Card GetNewCard()
    {
        if(currentDeck.cards.Count == 0)
        {
            GetNewCurrentDeck();
        }
        Card card = currentDeck.cards[0];
        releaseDeck.cards.Add(card);
        currentDeck.cards.RemoveAt(0);
        //Debug.Log("New card is " + card.imgPath + ", " + card.imgName);
        return card;
    }

    public void MergeTwoDecks()
    {
        int releaseCardsCount = releaseDeck.cards.Count;
        int currentCardsCount = currentDeck.cards.Count;
        for(int i=0; i< releaseCardsCount; i++)
        {
            int cardID = UnityEngine.Random.Range(0, releaseCardsCount - i);
            currentDeck.cards.Insert(UnityEngine.Random.Range(Mathf.Max(currentCardsCount - 10, 0), currentCardsCount + i), releaseDeck.cards[cardID]);
            releaseDeck.cards.RemoveAt(cardID);
        }
        Resources.UnloadUnusedAssets();
    }

    public void PlayCard()
    {
        playedCard = GetNewCard();
        cardSprite.color = tmpAlpha0;
        cardSprite.sprite = Resources.Load<Sprite>("CardsLibrary/" + playedCard.imgPath + "/" + playedCard.imgName);
        //cardSprite.sprite = playedCard.cardImg;
        cardSprite.DOFade(1f, 0.25f);
    }

    public void CheckAnswer(string answer)
    {
        if (isFirstTouch)
        {
            isFirstTouch = false;
            CountdownTimerScript.Instance.StartTimer();
        }
        if (String.Compare(answer, playedCard.imgPath, false) == 0) // answer.Equals(playedCard.imgPath)
        {
            //Debug.Log("Correct answer!");
            AudioController.Play("CorrectSound");
            GameManager.Instance.AppendScore();
        }
        else
        {
            //Debug.Log("Incorrect answer");
            AudioController.Play("IncorrectSound");
            GameManager.Instance.DeductLive();
        }
        PlayCard();
    }

}
