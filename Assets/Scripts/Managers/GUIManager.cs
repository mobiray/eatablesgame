﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class GUIManager : MonoBehaviour {

    public RectTransform GameFieldPanel;
    public RectTransform ModesPanel;
    public RectTransform MenuControlPanel;
    public RectTransform SettingsPanel;
    public Button ModesButton;
    public Text MainLabel;
    public Text[] RecordTexts;
    public Text GameModeText;
    public Image GameModeInfinityImage;
    public Text NoTimeLimitText;

    //public Text LivesText;
    public Transform TimeModesPanel;
    public Transform LivesPanel;
    public Text ScoreText;
    public Text MenuScoreText;
    public Text BestScoreText;

    private GameObject currentPanel;

    static public GUIManager Instance { get { return _instance; } }
    static protected GUIManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        RefreshRecordTexts();
    }

    public void SetMenuScoreTexts()
    {
        if (ModesController.Instance.currentMode == ModesController.GameMode.NoTimeLimit)
        {
            MenuScoreText.text = Convert.ToString(PlayerPrefs.GetInt("LastScore" + (int)ModesController.Instance.currentMode, 0) / 10f + "%");
            BestScoreText.text = Convert.ToString(PlayerPrefs.GetInt("BestScore" + (int)ModesController.Instance.currentMode, 0) / 10f + "%");
        } else
        {
            MenuScoreText.text = Convert.ToString(PlayerPrefs.GetInt("LastScore" + (int)ModesController.Instance.currentMode, 0));
            BestScoreText.text = Convert.ToString(PlayerPrefs.GetInt("BestScore" + (int)ModesController.Instance.currentMode, 0));
        }
        
    }

    //public void SetLivesText(string text)
    //{
    //    LivesText.text = text;
    //}

    public void DeductLive()
    {
        for (int i=0; i<LivesPanel.childCount; i++)
        {
            if (LivesPanel.GetChild(i).gameObject.activeSelf)
            {
                LivesPanel.GetChild(i).gameObject.SetActive(false);
                break;
            }
        }
    }

    public void RefreshLives()
    {
        for(int i=0; i<LivesPanel.childCount; i++)
        {
            LivesPanel.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void SetGameModeTextOrImage(ModesController.GameMode gm)
    {
        switch (gm)
        {
            case ModesController.GameMode.NoTimeLimit:
                GameModeText.color = new Color(1f, 1f, 1f, 0f);
                GameModeInfinityImage.color = Color.white;
                break;
            case ModesController.GameMode.ShortGame:
                GameModeText.color = Color.white;
                GameModeText.text = "30";
                GameModeInfinityImage.color = new Color(1f, 1f, 1f, 0f);
                break;
            case ModesController.GameMode.LongGame:
                GameModeText.color = Color.white;
                GameModeText.text = "60";
                GameModeInfinityImage.color = new Color(1f, 1f, 1f, 0f);
                break;
        }
    }

    public void SetTimeModesPanel(bool state)
    {
        if (state)
        {
            TimeModesPanel.GetComponent<CanvasGroup>().alpha = 1;
            NoTimeLimitText.color = new Color(0f, 0f, 0f, 0f);
            ScoreText.transform.parent.GetComponent<RectTransform>().anchoredPosition = new Vector2(-22f, -35f);
        } else
        {
            TimeModesPanel.GetComponent<CanvasGroup>().alpha = 0;
            NoTimeLimitText.color = new Color32(32, 17, 7, 255);
            ScoreText.transform.parent.GetComponent<RectTransform>().anchoredPosition -= new Vector2(185f, 0f);
        }
    }

    public void SetScoreText(int score)
    {
        if (score == 0)
        {
            ScoreText.GetComponent<RectTransform>().sizeDelta = new Vector2(90, 177);
        }
        if (score == 10 || score == 100)
        {
            ScoreText.GetComponent<RectTransform>().sizeDelta += new Vector2(55, 0);
        }
        ScoreText.text = score.ToString();
    }

    public void SetGameFieldPanel(bool state)
    {
        GameFieldPanel.gameObject.SetActive(state);
    }

    public void SetMenuControlPanel(bool state)
    {
        MenuControlPanel.gameObject.SetActive(state);
        if (!state)
        {
            MenuControlPanel.GetComponent<MenuMovement>().SetInitialPositions();
        }
        //if (state)
        //{
        //    MenuScoreText.text = Convert.ToString(PlayerPrefs.GetInt("LastScore", 0));
        //}
    }

    public void SetSettingsScreen(bool state)
    {
        if (state)
        {
            if (GameObject.Find("ModesPanel"))
            {
                currentPanel = ModesPanel.gameObject;
            } else
            {
                currentPanel = MenuControlPanel.gameObject;
            }
        }
        currentPanel.SetActive(!state);
        SettingsPanel.gameObject.SetActive(state);
    }

    public void SetModesPanel(bool state)
    {
        ModesPanel.gameObject.SetActive(state);
        if (state)
        {
            RefreshRecordTexts();
        }
    }

    //public void SetBestScoreText()
    //{
    //    BestScoreText.text = Convert.ToString(PlayerPrefs.GetInt("BestScore", 0));
    //}

    public void SetModesButtonInteractable(bool state)
    {
        ModesButton.interactable = state;
    }

    public void SetMainLabelText(string labelText)
    {
        MainLabel.text = labelText;
    }

    private void RefreshRecordTexts()
    {
        int panelsCount = ModesPanel.GetComponent<MenuMovement>().Panels.Length;
        if(panelsCount == RecordTexts.Length)
        {
            RecordTexts[0].text = Convert.ToString(PlayerPrefs.GetInt("BestScore0", 0) / 10f + "%");
            for (int i = 1; i < panelsCount; i++)
            {
                RecordTexts[i].text = PlayerPrefs.GetInt("BestScore" + i, 0).ToString();
            }
        }
       
    }
}
