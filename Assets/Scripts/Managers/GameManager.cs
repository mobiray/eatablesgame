﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {

    public int totalLives { get; protected set; }
    public int currentLives { get; protected set; }
    public int score { get; protected set; }
    private int incorrectScore = 0;

    private int gamesInARowCount = 0;

    private int bestScore;
    [HideInInspector]
    public bool haveTimeLimit = true;

    static public GameManager Instance { get { return _instance; } }
    static protected GameManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }

        //bestScore = PlayerPrefs.GetInt("BestScore", 0);
    }

    void Start()
    {
        SetUpMusic();
    }

    public void RefreshBestScore()
    {
        bestScore = bestScore = PlayerPrefs.GetInt("BestScore" + (int)ModesController.Instance.currentMode, 0);
    }

    public void StartRound()
    {
        //UPDATE: Replace to AdvertismentScript
        AdvertismentScript.Instance.HideBannerTop();
        gamesInARowCount++;
        score = 0;
        incorrectScore = 0;
        totalLives = 3;
        currentLives = totalLives;
        GUIManager.Instance.RefreshLives();
        GUIManager.Instance.SetScoreText(score);
        //CardsManager.Instance.PlayCard();
    }



    public void DeductLive()
    {
        if (haveTimeLimit)
        {
            /*Add condition currentLives >= 0*/
            if (currentLives > 0)
            {
                currentLives--;
                GUIManager.Instance.DeductLive();
            }
            else
            {
                FinishRound();
            }
        } else
        {
            incorrectScore++;
        }
    }

    public void CancelRound()
    {
        GUIManager.Instance.SetMenuControlPanel(true);
        GUIManager.Instance.SetGameFieldPanel(false);
        CardsManager.Instance.MergeTwoDecks();
        FlurryServiceScript.Instance.LogFlurryEvent("Game is canceled");
        AdvertismentScript.Instance.ShowInterstitial();
        AdvertismentScript.Instance.ShowBannerTop();
    }

    public void FinishRound()
    {
        SaveScores();
        GUIManager.Instance.SetMenuControlPanel(true);
        GUIManager.Instance.SetGameFieldPanel(false);
        CardsManager.Instance.MergeTwoDecks();
        AdvertismentScript.Instance.ShowInterstitial();
        AdvertismentScript.Instance.ShowBannerTop();
        GPlayManager.Instance.UnlockTotalGamesAchievment();
        GPlayManager.Instance.UnlockRoundScoreAchievment(score);
        FlurryServiceScript.Instance.LogFlurryEvent("Games in a row count.");
    }

    public void AppendScore()
    {
        score++;
        GUIManager.Instance.SetScoreText(score);
        if(!haveTimeLimit && score == 30)
        {
            FinishRound();
        }
    }

    private void SaveScores()
    {
        if (!haveTimeLimit)
        {
            score = Mathf.RoundToInt(score * 1000f / (score + incorrectScore));
        }
        PlayerPrefs.SetInt("LastScore" + (int)ModesController.Instance.currentMode, score);
        if (bestScore < score)
        {
            bestScore = score;
            PlayerPrefs.SetInt("BestScore" + (int)ModesController.Instance.currentMode, bestScore);
            //GPlayManager.Instance.UnlockRoundScoreAchievment(score);
        }
        GUIManager.Instance.SetMenuScoreTexts();
        StatisticsManager.Instance.AddNewLine(score);
        GPlayManager.Instance.ReportScore(score);
    }

    private void SetUpMusic()
    {
        AudioController.PlayMusic("BackgroundMusic");
        if (PlayerPrefs.GetInt("Music", 1) != 1)
        {
            AudioController.SetCategoryVolume("Music", 0f);
        }
        if (PlayerPrefs.GetInt("SFX", 1) != 1)
        {
            AudioController.SetCategoryVolume("SFX", 0f);
        }
    }

}
