﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonsShowing : MonoBehaviour {

    private Button btn;
    void Awake()
    {
        btn = this.GetComponent<Button>();
    }

    void OnEnable()
    {
        if(btn.interactable == false)
        {
            StartCoroutine(MakeInteractable());
        }
    }

    void OnDisable()
    {
        btn.interactable = false;
    }

    public IEnumerator MakeInteractable()
    {
        yield return new WaitForSecondsRealtime(0.5f);
        btn.interactable = true;
    }
}
