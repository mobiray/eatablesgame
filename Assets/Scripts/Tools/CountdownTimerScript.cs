﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CountdownTimerScript : MonoBehaviour {

    /*Duration of timer in seconds. Should be less than 30 ATM*/
    public int durationTime = 30;

    private Text timerText;
    private Color32 timerColor = new Color32(54, 29, 12, 255);
    private string startingLine = "00:";
    private float currentTime;
    private int currentSecond;
    private bool shouldStart = false;
    private bool shouldCheckTime = true;

    static string[] stringsFrom00To60 = {
        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
        "20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
        "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
        "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
        "50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
        "60"
    };

    static public CountdownTimerScript Instance { get { return _instance; } }
    static protected CountdownTimerScript _instance;

    void Awake()
    {
        _instance = this;
        timerText = this.GetComponent<Text>();
        //sb = new StringBuilder(5);
    }

    void OnEnable()
    {
        CardsManager.Instance.isFirstTouch = true;
        shouldStart = false;
        switch (ModesController.Instance.currentMode)
        {
            case ModesController.GameMode.ShortGame:
                timerText.color = timerColor;
                durationTime = 30;
                shouldCheckTime = true;
                timerText.text = startingLine + stringsFrom00To60[durationTime];
                break;
            case ModesController.GameMode.LongGame:
                timerText.color = timerColor;
                durationTime = 60;
                shouldCheckTime = true;
                timerText.text = "01:00";
                break;
            case ModesController.GameMode.NoTimeLimit:
                timerText.color = new Color(0f, 0f, 0f, 0f);
                durationTime = 60;
                shouldCheckTime = false;
                break;
        }
        
        currentSecond = durationTime;
        currentTime = durationTime;
    }

    void Update()
    {
        if (shouldStart && shouldCheckTime)
        {
            currentTime -= Time.deltaTime;
            if((int)currentTime < currentSecond)
            {
                currentSecond = (int)currentTime;
                timerText.text = startingLine + stringsFrom00To60[currentSecond];
            }
            
            if(currentTime <= 0)
            {
                GameManager.Instance.FinishRound();
                shouldStart = false;
            }
        }
    }

    public void StartTimer()
    {
        shouldStart = true;
    }

}
