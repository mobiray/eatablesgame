﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlurryServiceScript : MonoBehaviour {

#if UNITY_ANDROID
    private string FLURRY_API = "5B83438MWPV87TFVR57Q";
#elif UNITY_IPHONE
	private string FLURRY_API = "MYSB2HCSHBXBXX3H9FN7";
#else
	private string FLURRY_API = "x";
#endif
    static public FlurryServiceScript Instance { get { return _instance; } }
    static protected FlurryServiceScript _instance;

    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        FlurryAgent.Instance.onStartSession(FLURRY_API);

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        //FlurryAgent.Instance.logEvent("Application is opened");
    }

    public void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            FlurryAgent.Instance.onEndSession();
        }
        else
        {
            FlurryAgent.Instance.onStartSession(FLURRY_API);
        }
    }

    public void LogFlurryEvent(string flurryEvent)
    {
        FlurryAgent.Instance.logEvent(flurryEvent);
    }

    public void LogFlurryEvent(string flurryEvent, Dictionary<string, string> dic)
    {
        FlurryAgent.Instance.logEvent(flurryEvent, dic);
    }

    public void OnApplicationQuit()
    {
        FlurryAgent.Instance.onEndSession();
    }
}
