﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleController : MonoBehaviour {

    public void OnEatableToggleClick()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            CardsManager.Instance.CheckAnswer("Eatable");
        }
    }

    public void OnUneatableToggleClick()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            CardsManager.Instance.CheckAnswer("Uneatable");
        }
    }
}
