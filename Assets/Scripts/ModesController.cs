﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SunCubeStudio.Localization;

public class ModesController : MonoBehaviour
{
    public enum GameMode { NoTimeLimit, LongGame, ShortGame };
    private string[] modeNames = new string[] { "game_mode_1", "game_mode_2", "game_mode_3" };

    public GameMode currentMode { get; private set; }

    static public ModesController Instance { get { return _instance; } }
    static protected ModesController _instance;

    private void Awake()
    {
        _instance = this;
        currentMode = GameMode.LongGame;
    }

    private void Start()
    {
    }

    public void SetGameMode(int modeID)
    {
        if((int)currentMode != modeID)
        {
            currentMode = (GameMode) modeID;
        }
    } 

    public string GetCurrentModeName()
    {
        return LocalizationService.Instance.GetTextByKey(modeNames[(int)currentMode]);
    }

    public void SetCurrentMode(int modeID)
    {
        currentMode = (GameMode)modeID;
    }
}
