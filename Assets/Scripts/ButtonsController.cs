﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsController : MonoBehaviour {

    public void OnStartGameButtonClick()
    {
        GameManager.Instance.StartRound();
        GUIManager.Instance.SetMenuControlPanel(false);
        GUIManager.Instance.SetGameFieldPanel(true);
    }

    public void OnSettingsButtonClick()
    {
        GUIManager.Instance.SetSettingsScreen(true);
    }

    public void OnChoosedModeButtonClick()
    {
        GUIManager.Instance.SetModesPanel(false);
        GUIManager.Instance.SetMenuScoreTexts();
        StatisticsManager.Instance.LoadStatistics();
        GameManager.Instance.RefreshBestScore();
        GameManager.Instance.haveTimeLimit = (ModesController.Instance.currentMode != ModesController.GameMode.NoTimeLimit);
        GUIManager.Instance.SetMainLabelText(ModesController.Instance.GetCurrentModeName());
        GUIManager.Instance.SetGameModeTextOrImage(ModesController.Instance.currentMode);
        GUIManager.Instance.SetMenuControlPanel(true);
        GUIManager.Instance.SetTimeModesPanel(ModesController.Instance.currentMode != ModesController.GameMode.NoTimeLimit);
    }

    public void OnBackToModesButtonClick()
    {
        GUIManager.Instance.SetMenuControlPanel(false);
        GUIManager.Instance.SetModesPanel(true);
    }

    public void OnSettingsBackButtonClick()
    {
        GUIManager.Instance.SetSettingsScreen(false);
    }

    public void OnAchievmentsButtonClick()
    {
       // Debug.Log("OnAchievmentsButtonClick");
        GPlayManager.Instance.ShowAchievmentsUI();
        FlurryServiceScript.Instance.LogFlurryEvent("Achievment button was clicked.");
    }

    public void OnRateUsButtonClick()
    {
        /*TODO: CHANGE URL*/
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.twodtwob.eatable");
        FlurryServiceScript.Instance.LogFlurryEvent("Rate us button was clicked.");
    }

    public void OnPrivacyPolicyButtonClick()
    {
        Application.OpenURL("https://2d2b.github.io/privacy/com.twodtwob.eatable.html");
        FlurryServiceScript.Instance.LogFlurryEvent("Privacy policy button was clicked.");
    }

    public void OnLeaderboardButtonClick()
    {
        // Debug.Log("OnLeaderboardButtonClick");
        GPlayManager.Instance.ShowLeaderboardUI();
    }

    public void OnCancelGameButtonClick()
    {
        GameManager.Instance.CancelRound();
    }

    public void OnCommonLeaderboardButtonClick()
    {
        Social.ShowLeaderboardUI();
    }
}
