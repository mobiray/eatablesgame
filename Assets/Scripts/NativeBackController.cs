﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NativeBackController : MonoBehaviour {

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameObject settingsGO = GameObject.Find("SettingsPanel");
            if (settingsGO)
            {
                GUIManager.Instance.SetSettingsScreen(false);
            }
            else if(GameObject.Find("MenuControlPanel"))
            {
                GUIManager.Instance.SetMenuControlPanel(false);
                GUIManager.Instance.SetModesPanel(true);
            } else if (GameObject.Find("GameFieldPanel"))
            {
                GameManager.Instance.CancelRound();
            }
        }
    }

}
