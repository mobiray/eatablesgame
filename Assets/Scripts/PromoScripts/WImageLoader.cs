﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.IO;

// Get the latest webcam shot from outside "Friday's" in Times Square
public class WImageLoader : MonoBehaviour
{
    public string hardcodedBunleID;
    public Texture2D hardcodedTexRu;
    public Texture2D hardcodedTexEn;
    private ModelsAndDescAds.Adv[] myModelsAndDescAds;
    private Texture2D[] promoTextures;
    private string url = "https://raw.githubusercontent.com/enckrumlofj/ads2/master/ads.jso";
    private Texture2D hardcodedTex;

    private string promoPath;
    private RawImage promoImage;
    private int currentPromoID = -1;
    private string bundleID = "";
    private bool promoIsSet = false;

    private void Awake()
    {
        promoPath = Application.persistentDataPath + "/2D2B_Promo";
        promoImage = transform.GetChild(1).GetComponent<RawImage>();
        bundleID = hardcodedBunleID;
        if(Application.systemLanguage == SystemLanguage.Russian)
        {
            hardcodedTex = hardcodedTexRu;
        } else
        {
            hardcodedTex = hardcodedTexEn;
        }
        string language = Application.systemLanguage == SystemLanguage.Russian ? "ru" : "en";
        url = "https://raw.githubusercontent.com/2d2b/AdsRepo/master/" + Application.identifier + "." + language + ".json";

        promoImage.texture = hardcodedTex;
    }

//    IEnumerator Start()
//    {
//        WWW www = new WWW(url);
//        yield return www;
//
//		string jsonString = www.text;
//        ModelsAndDescAds mAndDesc2 = JsonUtility.FromJson<ModelsAndDescAds>(jsonString);
//        myModelsAndDescAds = mAndDesc2.ads;
//        promoTextures = new Texture2D[myModelsAndDescAds.Length + 1];
//        promoTextures[0] = hardcodedTex;
//        currentPromoID = (DateTime.Now.DayOfYear - 1) % promoTextures.Length;
//        if (!File.Exists(promoPath))
//        {
//            Directory.CreateDirectory(promoPath);
//        }
//
//        if (PlayerPrefs.HasKey("PROMO_STRING") && String.Compare(jsonString, PlayerPrefs.GetString("PROMO_STRING", "")) != 0)
//        {
//            ModelsAndDescAds tmpMAD = JsonUtility.FromJson<ModelsAndDescAds>(PlayerPrefs.GetString("PROMO_STRING"));
//            for(int i=0; i<tmpMAD.ads.Length; i++)
//            {
//                File.Delete(promoPath + "/" + i + ".png");
//            }
//            PlayerPrefs.SetString("PROMO_STRING", jsonString);
//            
//        }
//        for (int i = 0; i < myModelsAndDescAds.Length; i++)
//        {
//            if(File.Exists(promoPath + "/" + i + ".png"))
//            {
//                byte[] bytes = File.ReadAllBytes(promoPath + "/" + i + ".png");
//                if (bytes != null)
//                {
//                    promoTextures[i + 1] = new Texture2D(2, 2);
//                    promoTextures[i + 1].LoadImage(bytes);
//                }
//            } else
//            {
//                StartCoroutine(LoadImage(i));
//            }
//        }
//    }

//    private void Update()
//    {
//        if(Application.internetReachability == NetworkReachability.NotReachable)
//        {
//            return;
//        }
//        if(!promoIsSet)
//        {
//            if (promoTextures[currentPromoID] != null)
//            {
//                promoImage.texture = promoTextures[currentPromoID];
//                bundleID = myModelsAndDescAds[currentPromoID - 1].package_id;
//                promoIsSet = true;
//            }
//        }
//    }

    IEnumerator LoadImage(int index)
    {
        WWW www = new WWW(myModelsAndDescAds[index].icon);
        yield return www;
        promoTextures[index + 1] = www.texture;
        File.WriteAllBytes(promoPath + "/" + index + ".png", promoTextures[index + 1].EncodeToPNG());
    }

    public void OnWeAlsoMadeButtonClick()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + bundleID);
        FlurryServiceScript.Instance.LogFlurryEvent("We also made button was clicked.");
    }
}
