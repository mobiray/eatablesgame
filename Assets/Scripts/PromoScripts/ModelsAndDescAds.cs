﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct ModelsAndDescAds
{
    [System.Serializable]
    public struct Adv
    {
        public int order;
        public string package_id;
        public string icon;
        public string name;
    }
    public Adv[] ads;
}
